var express = require('express');

process.on('SIGINT', function() {
    process.exit();
});

var request = require('request');

var app = express();

app.use(express.static('static'));

app.use(require('body-parser').json());

var menu = {
    items: [{
        name: 'BBQ Beef Brisket Sandwich',
        price: 5.49
    }, {
        name: 'Italian Sausage Sandwich',
        price: 5.99
    }, {
        name: 'Chicken And Ribs',
        price: 12.99
    }]
};


app.get('/menu', (req, res) => {
    res.json(menu);
});

app.post('/submit', (req, res) => {
    let order = req.body;
    
    console.log("submitting order to storefront: " + JSON.stringify(order));
    
    var options = {
        json: order
    };
    
    request.post('http://172.17.0.2:9050/submit', options, (err, response, body) => {
        if (err) {
            console.error(err);
            
            return res.status(500).json({
                message: 'error sending order to storefront'
            });
        }
        
        if (response.statusCode != 200) {
            return res.status(500).json({
                message: 'storefront returned a status code of ' + response.statusCode
            });
        }
        
        console.log("response from storefront: " + response.statusCode);
        
        res.json({
            message: 'ok'
        });
    });
});

let port = 9070;

console.info('starting up scrounge-mobile');

app.listen(port, () => {
    console.info("scrounge-mobile up on port %s", port);
});
