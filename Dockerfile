FROM node:6.10

COPY package.json /opt/scrounge/

COPY server.js /opt/scrounge/

COPY static/*.html /opt/scrounge/static/

WORKDIR /opt/scrounge

RUN npm install

ENTRYPOINT node server.js
