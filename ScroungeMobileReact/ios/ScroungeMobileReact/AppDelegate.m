/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>

//added for react-native-navigation
#import "RCCManager.h"

#import <React/RCTRootView.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;
  
#ifdef DEBUG
  
  //uncomment this line if not at Jared's apartment for his macbook, (reserved ip and ports for communication-google router)
//    jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
  
  //Use this line if at Jared's apartment to use debug and hot reload, (reserverd ip and ports for communication-google router)
//  jsCodeLocation = [NSURL URLWithString:@"http://192.168.86.62:8081/index.ios.bundle?platform=ios&dev=true"];
  //use this line for dans place
//  jsCodeLocation = [NSURL URLWithString:@"http://192.168.1.10:8081/index.ios.bundle?platform=ios&dev=true"];
  
  jsCodeLocation = [NSURL URLWithString:@"http://192.168.1.250:8081/index.ios.bundle?platform=ios&dev=true"];
  
  
#else
   jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  self.window.backgroundColor = [UIColor whiteColor];
  [[RCCManager sharedInstance] initBridgeWithBundleURL:jsCodeLocation];
  
  return YES;
}

@end
