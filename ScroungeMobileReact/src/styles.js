import {
     StyleSheet
} from 'react-native'

export const styles = StyleSheet.create({
    
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: '#E6EDEC',
//        backgroundColor: '#F5FCFF',
    },
    
    map: {
        ...StyleSheet.absoluteFillObject
    },
      
    titleContainer: {
//        flex: 1,
        backgroundColor: '#9EA3A2',
//        alignItems: 'center',
//        justifyContent: 'center',
        padding: 12,
        paddingBottom: 20
    },
    
    simpleList: {
        flex: 1,
        
        backgroundColor: 'white'
    },
    
    listItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        
        backgroundColor: 'white',
        
        borderBottomWidth: 0.5,
        borderBottomColor: "lightgray",
    },
    
    truckListItem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        
        borderBottomWidth: 1,
        borderBottomColor: "lightgray",
    },
    
    title: {
        fontSize: 20, 
//      fontWeight: 'bold', 
//      fontFamily: 'MarkerFelt-Wide', 
        color: '#FFF'
    },
    
    paragraph: {
        fontSize: 12, 
//        fontFamily: 'MarkerFelt-thin', 
        color: '#FFF', 
        paddingTop: 6
    },
    
    listItemTitle: {
        fontSize: 18

//        fontFamily: 'MarkerFelt-Wide',
//        fontSize: 24,
//        color: '#434544',
//        fontWeight: 'bold',
    },
    
    listItemText: {
        marginTop: 6,
        marginLeft: 10,
        fontSize: 12

//        fontFamily: 'MarkerFelt-Wide',
//        fontSize: 18,
//        color: '#9EA3A2',
    },
    
    action: {
        
        fontSize: 18
        
    },
    
    imageIcon: {
        width: 20,
        height: 20
    },
    
    statusIcon: {
        width: 60,
        height: 60
    },
    
});
