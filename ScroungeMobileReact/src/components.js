import React, { Component } from 'react'

import {
    View,
    TouchableHighlight,
    Text,
    Image
} from 'react-native';

import { styles } from './styles';

export class TouchableListItem extends Component {
    
    render() {
        if (this.props.onTouch) {
            return (
                <TouchableHighlight onPress={ this.props.onTouch }>
                  <View style={ styles.listItem }>
                    <Text style={[ styles.listItemTitle, { paddingLeft: 12 } ]}>{ this.props.title }</Text>
                  
                    <DrillInto />
                  </View>
                </TouchableHighlight>
            );
        }
        
        return (
            <View style={[ styles.listItem, { paddingTop: 12, paddingBottom: 12 }]}>
              <Text style={[ styles.listItemTitle, { paddingLeft: 12 } ]}>{ this.props.title }</Text>
            </View>
        );
    }
    
}

export class DrillInto extends Component {
    
    render() {
        return (
            <Text style={{ fontSize: 20, color: 'lightgray', margin: 12 }}>></Text>
        );
    }
    
}

export class ImageButton extends Component {
    
    render() {
        return (
            <TouchableHighlight style={{ padding: 10 }} underlayColor='white' onPress={ this.props.onTouch }>
              <Image style={[ styles.imageIcon, {} ]} source={ this.props.resource } />
            </TouchableHighlight>
        );
    }
    
}