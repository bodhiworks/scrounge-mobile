import { Navigation } from 'react-native-navigation';
import Trucks from './trucks';
import Truck from './truck';
import Menu from './menu';
import Order, {
    OrderWait,
    OrderConfirmed,
    OrderDelivered
} from './order';

Navigation.registerComponent('scrounge.Trucks', () => Trucks);
Navigation.registerComponent('scrounge.Truck', () => Truck);
Navigation.registerComponent('scrounge.Menu', () => Menu);
Navigation.registerComponent('scrounge.Order', () => Order);
Navigation.registerComponent('scrounge.OrderWait', () => OrderWait);
Navigation.registerComponent('scrounge.OrderConfirmed', () => OrderConfirmed);
Navigation.registerComponent('scrounge.OrderDelivered', () => OrderDelivered);

Navigation.startSingleScreenApp({
    screen: {
//        screen: 'scrounge.Order', 
        screen: 'scrounge.Trucks', 
//        screen: 'scrounge.Menu', 
        title: 'Ballston', 
        navigatorStyle: {
            navBarBackgroundColor: '#F9B646',
            navBarTextColor: 'white',
            navBarSubtitleColor: 'lightgray',
        }, 
        navigatorButtons: {},
        
        
        
//        passProps: {
//            menu: {
//                items: []
//            }
//        }
    }
});
