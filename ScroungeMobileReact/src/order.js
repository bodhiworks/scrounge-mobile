import React, { Component } from 'react';

import {
    View,
    ScrollView,
    Text,
    Image,
    Button,
    ActivityIndicator
} from 'react-native'

import { TouchableListItem } from './components'

import { styles } from './styles'

import minusIcon from './images/minus.png';
import checkIcon from './images/check.png';

let api = require('./lib/api');
const OrderFactory = require('./lib/order');

class OrderItem extends Component {
    
    render() {
        // from TouchableListItem
        return (
            <View style={[ styles.listItem, { paddingTop: 12, paddingBottom: 12 }]}>
              <Text style={[ styles.listItemTitle, { paddingLeft: 12 } ]}>{ "my order item" }</Text>
              
              <Image style={[ styles.imageIcon, { marginRight: 20 } ]} source={ minusIcon } />
            </View>
        );
    }
    
}

export class OrderWait extends Component {
    
    componentDidMount() {
        var nav = this.props.navigator;
        
        var order = this.props.order;
        
        setTimeout(() => {
            console.log("sim order confirmed");
            
            nav.dismissLightBox();
            
            nav.resetTo({
                screen: 'scrounge.OrderConfirmed',
                navigatorStyle: {
                    navBarHidden: true,
                },
                passProps: {
                    order: order
                },
            });
        }, 1000);
    }
    
    render() {
        return (
            <ActivityIndicator
                animating={true}
                size="large"
                color="white"
                />
        );
    }
    
}
   
export class OrderConfirmed extends Component {
    
    constructor() {
        super();
        
        this.doSomethingElse = this.doSomethingElse.bind(this);
    }
    
    componentDidMount() {
        var nav = this.props.navigator;
        
        var order = OrderFactory.get(this.props.order);
        
        order.onStatusChange(this, () => {
            nav.resetTo({
                screen: 'scrounge.OrderDelivered',
                navigatorStyle: {
                    navBarHidden: true,
                },
                passProps: {
                },
            });
        });
    }
    
    doSomethingElse() {
        this.props.navigator.resetTo({
            screen: 'scrounge.Trucks',
            title: 'Ballston',
            passProps: {
            },
        });
    }
    
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'steelblue' }}>
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={ styles.statusIcon } source={ checkIcon } />
                <Text style={{ fontWeight: 'bold', color: 'white' }}>CONFIRMED</Text>
              </View>
              <Button title="Back" onPress={ this.doSomethingElse } />
            </View>
        );
    }
    
}

export class OrderDelivered extends Component {
    
    constructor() {
        super();
        
        this.doSomethingElse = this.doSomethingElse.bind(this);
    }
    
    doSomethingElse() {
        this.props.navigator.resetTo({
            screen: 'scrounge.Trucks',
            title: 'Ballston',
            passProps: {
            },
        });
    }
    
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'steelblue' }}>
              <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={ styles.statusIcon } source={ checkIcon } />
                <Text style={{ fontWeight: 'bold', color: 'white' }}>READY FOR PICKUP</Text>
              </View>
              <Button title="Back" onPress={ this.doSomethingElse } />
            </View>
        );
    }
    
}

export default class Order extends Component {
    
    componentDidMount() {
//        api.storefrontHealth();
    }
    
    submitOrder() {
        let order = OrderFactory.create({
            customer: "Billy Doe",
            
            items: [ {name: "brisket"}, {name: "mac n cheese"}, {name: "Pork n Beans"} ]
        });
        
        var nav = this.props.navigator;
        
        
        /*
         * TODO: the logic here should be
         * 
         *      1. POST the order
         *      2. show the wait screen
         *      3. when 'accepted' message received, show confirm screen
         */
        
        order.submit()
            .then(function() {
                nav.showLightBox({
                    screen: 'scrounge.OrderWait',
                    passProps: {
                        order: order.id
                    },
                    style: {
                        backgroundBlur: "dark", // 'dark' / 'light' / 'xlight' / 'none' - the type of blur on the background
    //                backgroundColor: "#ff000080" // tint color for the background, you can specify alpha here (optional)
                    }
                });
            })
            .catch((error) => {
                console.log('could not submit order: '+ JSON.stringify(error));
            })
            ;
    }
    
    render() {
        let truck = {
            
            name: 'Astro Chicken & Donuts',
            description: '$10.99'
        }
        
        return (
            <View style={{ flex: 1 }}>
            <View style={ styles.titleContainer }>
            <Text style={ styles.title }>{ truck.name }</Text>
            
            <Text style={ styles.paragraph }>{ truck.description }</Text>
            </View>
              <View style={{ backgroundColor: 'lightgray' }}>
              </View>

            
              <ScrollView style={{ flex: 1 }}>
                <OrderItem />
                <OrderItem />
                <OrderItem />
                <OrderItem />
              </ScrollView>
              
              <View style={{ backgroundColor: 'lightgray' }}>
                <Button style={ styles.action } title="Submit" onPress={ () => this.submitOrder() }/>
              </View>
            </View>
        );
    }
    
}