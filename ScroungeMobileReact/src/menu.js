import React, { Component } from 'react'

import {
    View,
    TouchableHighlight,
    Text,
    Image,
    ListView
} from 'react-native';

import { TouchableListItem, DrillInto, ImageButton } from './components';

import { styles } from './styles';

import plusIcon from './images/plus.png';

class MenuItem extends Component {
    
    constructor() {
        super();
        
        this.addToOrder = this.addToOrder.bind(this);
    }
    
    componentDidMount() {
    }
    
    addToOrder() {
        console.log("adding to order");
        
        this.props.temp();
    }
    
    render() {
        let item = this.props.item;
        
        return (
            <View style={ styles.listItem }>
              <View style={{ flex: 1, flexDirection: 'column', padding: 12 }}>
                <Text style={ styles.listItemTitle }>{ item.name }</Text>
              
                { item.description && <Text style={ styles.listItemText }>{ item.description }</Text> }
              </View>
              
              { item.items && <DrillInto /> }
              
              { !item.items &&
                <View style={{ paddingRight: 12 }}>
                  <ImageButton resource={ plusIcon } onTouch={ this.addToOrder } />
                </View>
              }
            </View>
        );
    }
    
}

export default class Menu extends Component {
        
    constructor() {
        super();
        
        this.showMenuCategory = this.showMenuCategory.bind(this);
        this.renderMenuItem = this.renderMenuItem.bind(this);
        this.reviewOrder = this.reviewOrder.bind(this);
        
        this.menuDataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        
        this.state = {
            menuRows: this.menuDataSource.cloneWithRows([])
        };
    }
    
    componentDidMount() {
        if (this.props.menu.items) {
            this.setState({
                menuRows: this.menuDataSource.cloneWithRows(this.props.menu.items)
            });
        }
    }
    
    reviewOrder() {
        console.log("review order");

        this.props.navigator.resetTo({
            screen: 'scrounge.Order',
            title: 'Order',
//            subtitle: this.state.truck.name,
//            passProps: {
//                truck: this.state.truck,
//                
//                menu: require('../testdata/menu')
//            }
        });
    }
    
    showMenuCategory(item) {
        this.props.navigator.push({
            screen: 'scrounge.Menu',
            title: item.name,
            subtitle: this.props.truck.name,
            passProps: {
                menu: item
            }
        });
    }
    
    renderMenuItem(item) {
        if (item.items) {
            return (
                <TouchableHighlight onPress={ () => { this.showMenuCategory(item) } }>
                  <View>
                    <MenuItem item={ item } temp={ () => console.log('WTF') } />
                  </View>
                </TouchableHighlight>
            );
        }
        
        return (
            <MenuItem item={ item } temp={ this.reviewOrder } />
        );
    }
    
    render() {
        return (
            <View style={ styles.container }>
              <View style={{ flex: 1 }}>
                <ListView
                    style={ styles.simpleList }
                    enableEmptySections={ true }
                    dataSource={ this.state.menuRows } 
                    renderRow={ this.renderMenuItem } />
              </View>
            </View>
        );
    }
    
}
