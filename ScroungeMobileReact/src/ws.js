import React, { Component } from 'react';

import { 
    Navigator,
    StyleSheet,
    Text,
    TouchableHighlight,
    View,
    } from 'react-native' ;

import MainApp from './MainApp';
import Waiting from './waiting'
import Trucks from './trucks';
import Truck from './truck';

import Something from './something';

export default class ScroungeMobileNavigator extends Component {
    constructor(){
        super();
        
        let that = this;
        
        this.messageHandlers = {
            newOrder: function(message) {
                console.log("received new order message");
            },
            
            updateCustomers: function(message) {
                // console.log("updating customers: " + JSON.stringify(message));
                
            }
        };
        this.connect = this.connect.bind(this);
        this.connect()
    }
    connect() {
        let host = 'network.scrounge.io';
        console.log('connecting to ws');
        
        let that = this;
        
        var ws = new WebSocket('wss://' + host);
        
        ws.onopen = () => {
            console.log(' ws is opened');
        };
        
        ws.onmessage = (e) => {
            let message = JSON.parse(e.data);
            
            let action = message.action;
            
            let f = that.messageHandlers[action];
            
            if (f) f(message);
            
            else console.log("no handler defined for message action " + action);
        };
        ws.onerror = (e) => {
            console.log(e.message);
        };

        ws.onclose = (e) => {
            console.log("closing");
        };
    }
    
    render() {
        return (
            <Navigator 
                initialRoute={{
                    title: 'Main',
                    
                    component: MainApp
                }}
            
                renderScene={ (route, navigator) =>
                    <View style={styles.window}>
                      <TitleBar title={ route.title } navigator={ navigator } />
                      <route.component navigator={ navigator } />
                    </View>
                }
             />
        );
    }
}

class TitleBar extends Component {
    render() {
        return (
            <View style={{width: '100%', backgroundColor: 'black', padding: 10}}>
                <View>
                    {/* <TouchableHighlight 
                        onPress={() => this.props.navigator.pop()}>
                      <Text style={{color: 'white', fontSize: 20}}>Back</Text>
                    </TouchableHighlight> */}
                    <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>{this.props.title}</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ccc',
        flex: 1,
    },
    title: {
        borderWidth: 2,
    },
    window: {
        flex: 1,
        // backgroundColor: 'blue',
    },
})


