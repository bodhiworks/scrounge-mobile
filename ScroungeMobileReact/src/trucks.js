import React, {Component} from 'react';

import {
    ActivityIndicator,
    Image,
    StyleSheet,
    TouchableOpacity,
    View,
    ListView,
    Text,
} from 'react-native';

import MapView from 'react-native-maps';
import FitImage from 'react-native-fit-image';
import { DrillInto } from './components';

import { styles } from './styles'

import api from './lib/api';

const region = {
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.015,
    longitudeDelta: 0.0121
}

class TruckListItem extends Component {
    
    render() {
        let truck = this.props.truck;
        
        return (
            <View style={ styles.truckListItem }>
              <Image
                  style={{ height: 72, width: 72 }} 
                  source={{ uri: truck.profile_image_url }} />
              
              <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 12 }}>
                <Text style={ styles.listItemTitle }>{ truck.name }</Text>
          
                <Text style={ styles.listItemText }>{ truck.cuisine ? truck.cuisine : 'Yummy' }</Text>
              </View>
        
              <DrillInto />
            </View>
        );
    }
    
}

export default class Trucks extends Component {

    constructor(props) {
        super(props);
        
        this.renderTruckItem = this.renderTruckItem.bind(this);
        this.seeTruck = this.seeTruck.bind(this);
        
        this.trucksDataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        
        this.state = {
            loading: true,
        };
    }
    
    componentWillMount() {
        let ds = this.trucksDataSource;
        
        api.getTrucks().then((trucks) => {
            this.setState({
                dataSource: ds.cloneWithRows(trucks),
                
                loading: false
            });
        });    
    }
    
    renderTruckItem(truck) {
        return (
          <TouchableOpacity
              onPress={ () => this.seeTruck(truck) }>
              
            <TruckListItem truck={ truck } />
          </TouchableOpacity>
        )
    }
    
    seeTruck(truck) {
        let nav = this.props.navigator;
        
        nav.push({
            screen: 'scrounge.Truck',
            title: truck.name,
            passProps: { truck: truck }
        });
    }
    
    render() {
        return (
            <View style={{ flex: 1 }}>  
              <View style={{ flex: 1 }}>
                <MapView style={ styles.map } region={ region } />
              </View>
                
              <View style={{ flex: 2 }}>
                { this.state.loading ?
                <ActivityIndicator
                    style={ styles.indicator }
                    animating={ this.state.loading }
                    size="large"
                    color="orange"
                    /> 
                :      
                 <ListView
                     dataSource={ this.state.dataSource } 
                     renderRow={ this.renderTruckItem } />
                }
              </View>
            </View>
        )
    }    
}
