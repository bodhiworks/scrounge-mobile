'use strict'

let EventEmitter = require('EventEmitter');

let orders = {
};

let api = require('./api');

let _emitter = new EventEmitter();

function createOrder(order) {
    var order = {
        id: 'order_' + new Date().getTime(),
        
        _componentListeners: {},
        
        onStatusChange: function(component, f) {
            var listeners = this._componentListeners;

            listeners[component.props.navigatorEventID] = f;

            component.props.navigator.setOnNavigatorEvent(function(e) {
                if ('willDisappear' == e.id && listeners[e.navigatorEventID]) {
                    _emitter.removeListener('status', listeners[e.navigatorEventID]);
                }
            });
      
            _emitter.addListener('status', f);
        },
        
        submit: function() {
            return api.submitOrder(order)
                .then(function() {
                    setInterval(function() {
                        _emitter.emit('status', { message: 'ok' });
                    }, 3000);
                    
                    return true;
                })
                ;
        }
    };
    
    orders[order.id] = order;
    
    return order;
}

let orderFactory = {
    
    get: (id) => {
        return orders[id];
    },
    
    create: createOrder
    
};

module.exports = orderFactory;