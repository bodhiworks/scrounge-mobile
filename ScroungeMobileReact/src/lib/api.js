/**
 * NOTE: setting up network interface is different emulator vs test device vs production
 * 
 * emulator: need to use 10.0.2.2, which maps to localhost
 *      - is "adb reverse" needed?
 *      
 * device:
 *      - use "adb reverse tcp:9050 tcp:9050" to map the interface
 *      - host is "localhost"
 * 
 * production:
 *      - just use production https endpoint
 */

//let orderingHost = 'http://10.0.2.2:9050';  // this is storefront local....
let orderingHost = 'http://localhost:9050';  // this is storefront local....
//let orderingHost = 'https://storefront.scrounge.io';  // this is storefront local....

const endpoint ='https://network.scrounge.io/api/store/';

const getOptions = {
    method: 'GET',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

const postOptions = {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
};

function createPost(body) {
    return Object.assign(postOptions, { body: JSON.stringify(body) });
}

const responseHandler = (response) => response.json();

module.exports = {
    
    getTrucks: function() {
        
        let promiseForTrucksResponse = fetch( endpoint , getOptions);
        
        let promiseForTrucks = promiseForTrucksResponse.then(responseHandler);
        
        promiseForTrucksResponse.catch( (error) => {
            console.log("Error loading the trucks " + JSON.stringify(error));
        })    
        
        return promiseForTrucks
    },
    
    getTruck: function(id) {
        
        let promiseForTruckResponse = 
        fetch(endpoint +`${id}` , getOptions);
        
        let promiseForTruck = promiseForTruckResponse.then(responseHandler);
        
        promiseForTruckResponse.catch( (error) =>{
            console.log('Error getting single truck ' + JSON.stringify(error) );
        })
        return promiseForTruck;
    },
    
    storefrontHealth: function(order) {
        return fetch(orderingHost + "/health", getOptions);
    },
    
    submitOrder: function(order) {
        return fetch(orderingHost + "/api/submit", createPost(order));
    }
    
}
