import React, { Component } from 'react';

import {
    Image,
    Linking,
    StyleSheet,
    View,
    ScrollView,
    Text,
    Button,
    TouchableOpacity,
} from 'react-native';

import api from './lib/api';
import FitImage from 'react-native-fit-image';
import Link from './link';
import { TouchableListItem, DrillInto } from './components';

import { styles } from './styles';

import twitter from './images/twitter-circle.png'
import globe from './images/globe.png'

class ExternalLinkListItem extends Component {
    
    render() {
        return (
            <TouchableOpacity
                style={[ styles.listItem, { paddingLeft: 20, paddingRight: 20, paddingTop: 12, paddingBottom: 12 } ]}
                onPress={ this.props.onTouch }>
            
              <Image style={{ height: 35, width: 35 }} source={ this.props.image } />
              
              <Text style={[ styles.listItemText, { flex: 1, paddingLeft: 10 } ]}>{ this.props.text }</Text>
            </TouchableOpacity>
        );
    }
    
}

class TwitterListItem extends ExternalLinkListItem {
    
    constructor() {
        super();
        
        this.handleTwitterClick = this.handleTwitterClick.bind(this);
    }
    
    handleTwitterClick() { 
        let twitter = `twitter://user?screen_name=${this.props.truck.screen_name}`;
        
        Linking.openURL(twitter)
            .catch(() => {
                Linking.openURL(`https://twitter.com/${this.props.truck.screen_name}`)
            })
            ;
    }
    
    render() {
        return (
            <ExternalLinkListItem image={ twitter } text={ this.props.truck.status.text } onTouch={ this.handleTwitterClick } />
        );
    }
    
}

class WebListItem extends Component {
    
    constructor() {
        super();
        
        this.handleWebsiteClick = this.handleWebsiteClick.bind(this);
    }
    
    handleWebsiteClick = () => { 
        let url = this.props.truck.url;
        
        Linking.canOpenURL(url).then(supported => {
            supported ? Linking.openURL(url) : console.log(`Don't know URL: ${url}`);
        });
    }
    
    render() {
        return (
            <ExternalLinkListItem image={ globe } text={ this.props.truck.url } onTouch={ this.handleWebsiteClick } />
        );
    }
    
}

export default class Truck extends Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
             loading: true
         };
        
        this.showMenu = this.showMenu.bind(this);
    }
    
    componentDidMount() { 
        api.getTruck(this.props.truck.id).then((truck) => {
            this.setState({
                truck: truck,
                
                loading: false
            });
        });
    }

    showMenu() {
        this.props.navigator.push({
            screen: 'scrounge.Menu',
            title: 'Menu',
            subtitle: this.state.truck.name,
            passProps: {
                truck: this.state.truck,
                
                menu: require('../testdata/menu')
            }
        });
    }
    
    render() {    
        let truck = this.state.truck;
        
        if (!truck) return <View />;
        
        return (
            <View style={ styles.container }>
              <View style={{ flex: 1 }}>
                <FitImage
                    indicator
                    indicatorColor="orange"
                    indicatorSize="large" 
                    source={{ uri: truck.profile_banner_url }}
                    /> 
              </View>
                
              <View style={{ flex: 2 }}>
                <ScrollView>
                  <View style={{ flex: -1 }}>
                    <View style={ styles.titleContainer }>
                      <Text style={ styles.title }>{ truck.name }</Text>
                                
                      <Text style={ styles.paragraph }>{ truck.description }</Text>
                    </View>
                      
                    { truck.status && <TwitterListItem truck={ this.state.truck } /> }
                    
                    { truck.url && <WebListItem truck={ this.state.truck } /> }
                    
                    <TouchableListItem title="Menu" onTouch={ this.showMenu } />
                  </View>
                </ScrollView>
              </View>
            </View>
        );
    }    
}
