import React, { Component } from 'react';
import {
    Image,
    Linking,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import api from './lib/api';
import FitImage from 'react-native-fit-image';


export default class Link extends Component {
    
    handleClick = () => { 
        Linking.openURL(this.props.url).catch( () => {
            Linking.openURL(`https://twitter.com/${url}`)
        })
    }
    
    render() {
        let image = this.props.img
        return (
            <View style={styles.linkContainer}>
                
                <Image
                    style={styles.imageContainer} 
                    source={require('./images/twitter-circle.png')}/>
                    
                <Text style={styles.displayText}> 
                    {this.state.truckData.status ? this.state.truckData.status.text : ''}
                </Text>
            </View>
            

        )
    }
}

const styles = StyleSheet.create({
    linkContainer: {
        backgroundColor: 'white',
        flexDirection: 'row', 
        alignItems: 'center', 
        paddingLeft: 20, 
        paddingRight: 20, 
        paddingTop: 12, 
        paddingBottom: 12, 
        borderBottomWidth: .5, 
        borderBottomColor:'#ccc'
    },
    imageContainer: {
        height: 60, 
        width: 60
    },
    displayText: {
        flex: 1, 
        paddingLeft: 20, 
        fontFamily: 'MarkerFelt-Wide', 
        fontSize: 18
    }
});