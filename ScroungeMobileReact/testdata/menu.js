let items = [{
    name: 'Sandwiches',
    
    items: [{
        name: 'Tripletas',
        description: 'Rosted pork, skirt steak, ham, lettuce, tomatoes, potatoes sticks, and our signature dressing served in pan sobao.'
    }, {
        name: 'Grilled Chicken',
        description: 'Marinated grilled chicken in pan sobao(PR bread) with lettuce and tomato and mayo ketchup.'
    }, {
        name: 'Cubano',
        description: 'Roasted pork, swiss cheese, pickles, and mustard served in pan de agua.'
    }],
}, {
    name: 'Soups',
    
    items: [{
        name: 'Sancocho',
        description: 'A beef hearty soup with root vegetables.'
    }, {
        name: 'Vegetable',
        description: 'A hearty soup made with fresh vegetable and roots vegetables.'
    }, {
        name: 'Asopao de Pollo',
        description: 'A gumbo style soup with rice, chicken, and root vegetables.'
    }]
}, {
    name: 'Sides',
    items: [{
        name: 'Alcapurrias',
        description: 'Beef-filled plantain fritters.'
    }, {
        name: 'Empanadillas',
        description: 'Stuffed pastry with Pizza, Chicken, Beef and Crab.'
    }]
}, {
    name: 'Beverages',
    items: [{
        name: 'Malta',
    }, {
        name: 'Coke',
    }, {
        name: 'Diet coke',
    }, {
        name: 'Juices (Passion Fruit, Guanabana, Tamarindo)',
    }, {
        name: 'Bottle of spring water'
    }]
}];

let menu = {
    name: 'default',
    
    items: items
};

module.exports = menu;